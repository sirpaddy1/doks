# Notes on my Arch Linux installation

- [Notes on my Arch Linux installation](#notes-on-my-arch-linux-installation)
  - [Preparation](#preparation)
  - [Pre-installation](#pre-installation)
    - [Set keyboard layout](#set-keyboard-layout)
    - [Select disk](#select-disk)
    - [Create the partitions](#create-the-partitions)
    - [Format EFI partition](#format-efi-partition)
    - [Encrypt system partition](#encrypt-system-partition)
    - [Backup LUKS header](#backup-luks-header)
    - [Open the encrypted system partition](#open-the-encrypted-system-partition)
    - [Encrypt swap partition](#encrypt-swap-partition)
    - [Create and mount root filesystem](#create-and-mount-root-filesystem)
    - [Mount EFI partition](#mount-efi-partition)
  - [Installation](#installation)
    - [Install essential packages](#install-essential-packages)
    - [Generate fstab](#generate-fstab)
    - [Boot into the system](#boot-into-the-system)
    - [Configure the system](#configure-the-system)
    - [Prepare UKI](#prepare-uki)
    - [Install systemd-boot](#install-systemd-boot)
    - [Configure systemd-boot](#configure-systemd-boot)
  - [Install microcode](#install-microcode)
  - [Setup Networking](#setup-networking)
    - [Simple Wired Network with DHCP](#simple-wired-network-with-dhcp)
    - [Simple Wired Network with Static IP](#simple-wired-network-with-static-ip)
    - [Simple Wireless Network with DHCP](#simple-wireless-network-with-dhcp)
  - [Installing Secure Boot](#installing-secure-boot)
  - [Using TPM 2.0](#using-tpm-20)
  - [TPM auto unlock for swap](#tpm-auto-unlock-for-swap)
- [Post-installation](#post-installation)
  - [Set up a wireless netwok](#set-up-a-wireless-netwok)
  - [Update pacman mirrors](#update-pacman-mirrors)
  - [Install window manager](#install-window-manager)
  - [Set up audio](#set-up-audio)
  - [Change TrackPoint sensitivity](#change-trackpoint-sensitivity)
  - [References](#references)

## Preparation

Boot up [Arch Linux ISO](https://archlinux.org/download/) and do the following:

* Disable the annoying beep sound: `rmmod pcspkr`
* Bring up WiFi via `iwctl station wlan0 connect <SSID>`
* Have some coffee ☕

## Pre-installation

### Set keyboard layout

```sh
loadkeys de-latin1
```

### Select disk

```sh
fdisk -l
```

(Use `fdisk` to determine the correct drive to install)

### Create the partitions

```sh
fdisk /dev/nvme0n1
```

Layout:

NAME        MAJ:MIN RM   SIZE RO TYPE  MOUNTPOINTS
nvme0n1     259:0    0 238,5G  0 disk  
|-nvme0n1p1 259:1    0   500M  0 part  
|-nvme0n1p2 259:2    0     8G  0 part  
| --swap    254:0    0     8G  0 crypt [SWAP]
|-nvme0n1p3 259:3    0   230G  0 part  
  --root    254:1    0   230G  0 crypt /


### Format EFI partition

```sh
mkfs.fat -F32 -n EFI /dev/nvme0n1p1
```

### Encrypt system partition

```sh
cryptsetup luksFormat /dev/nvme0n1p3
```

### Backup LUKS header

> If the header of a LUKS encrypted partition gets destroyed, you will not be able to decrypt your data. It is just as much of a dilemma as forgetting the passphrase or damaging a key-file used to unlock the partition. Damage may occur by your own fault while re-partitioning the disk later or by third-party programs misinterpreting the partition table. Therefore, having a backup of the header and storing it on another disk might be a good idea.

```sh
cryptsetup luksHeaderBackup /dev/nvme0n1p3 --header-backup-file /mnt/<backup>/<file>.img
```

### Open the encrypted system partition

```sh
cryptsetup open /dev/nvme0n1p3 root
```

### Encrypt swap partition

> To be able to resume after suspending the computer to disk (hibernate), it is required to keep the swap space intact. Therefore, it is required to have a pre-existent LUKS swap partition or file, which can be stored on the disk or input manually at startup.

```sh
cryptsetup luksFormat /dev/nvme0n1p2
cryptsetup open /dev/nvme0n1p2 swap
mkswap -L swap /dev/mapper/swap
swapon -L swap
```

> The following setup has the disadvantage of having to insert an additional passphrase for the swap partition manually on every boot.

However, we will eliminate this by storing the LUKS key in TPM.

### Create and mount root filesystem

```sh
mkfs.ext4 -L root /dev/mapper/root
mount LABEL=root /mnt
```

### Mount EFI partition

```sh
mkdir /mnt/efi
mount LABEL=EFI /mnt/efi
```

## Installation

### Install essential packages

```sh
pacstrap /mnt base linux linux-firmware iwd vim man-db man-pages texinfo 
```

### Generate fstab

```sh
genfstab -L /mnt >> /mnt/etc/fstab
```

```
-L
  Use labels for source identifiers (shortcut for -t LABEL).
```

```
# /dev/mapper/system UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
LABEL=root        	/         	ext4       	defaults	0 1

# /dev/nvme0n1p1 UUID=xxxx-xxxx
LABEL=EFI           	/efi     	vfat      	defaults,errors=remount-ro	0 2

# /dev/mapper/swap UUID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
#LABEL=swap          	none      	swap      	defaults  	0 0
# add this line instead for using the mapped device as swap
/dev/mapper/swap swap swap defaults 0 0
```

### Boot into the system

```sh
arch-chroot /mnt
```

### Configure the system

```sh
# Set the time zone
ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime

# Set the Hardware Clock from the System Clock, and update the timestamps in /etc/adjtime.
hwclock --systohc

# Uncomment desired locales
vim /etc/locale.gen
# Generate the locales
locale-gen

# Create the hostname
vim /etc/hostname

# Set the root password
passwd
```

### Prepare UKI

Create a backup of the original config:

```sh
cp /etc/mkinitcpio.conf /etc/mkinitcpio.conf.orig
```

Update `HOOKS` in `/etc/mkinitcpio.conf` as follows:

```
HOOKS=(systemd autodetect modconf kms keyboard sd-vconsole block sd-encrypt filesystems fsck)
```

Create directory for UKI:

```sh
mkdir -p /efi/EFI/Linux
```

Edit `/etc/kernel/cmdline`:

* `<ROOT-PARTITION-UUID>`: `lsblk -o NAME,UUID | grep nvme0n1p3 | awk '{print $NF}'`

```
rw quiet bgrt_disable rd.luks.name=<UUID-of-root-luks-container>=root root=/dev/mapper/root
```

Edit `/etc/kernel/fallback_cmdline`:

```
rw debug bgrt_disable rd.luks.name=<UUID-of-root-luks-container>=root root=/dev/mapper/root
```

Modify `/etc/mkinitcpio.d/linux.preset`:

* Un-comment the `default_uki=` and `fallback_uki=` parameters
* Comment out the `default_image=` and `fallback_image=` parameters
* Un-comment the `--splash` option for the default preset
* Add `--cmdline /etc/kernel/fallback_cmdline` the the `fallback_options`

should now look like this:

```
# mkinitcpio preset file for the 'linux' package

ALL_config="/etc/mkinitcpio.conf"
ALL_kver="/boot/vmlinuz-linux"
ALL_microcode=(/boot/*-ucode.img)

PRESETS=('default' 'fallback')

#default_config="/etc/mkinitcpio.conf"
#default_image="/boot/initramfs-linux.img"
default_uki="/efi/EFI/Linux/arch-linux.efi"
default_options="--splash /usr/share/systemd/bootctl/splash-arch.bmp"

#fallback_config="/etc/mkinitcpio.conf"
#fallback_image="/boot/initramfs-linux-fallback.img"
fallback_uki="/efi/EFI/Linux/arch-linux-fallback.efi"
fallback_options="-S autodetect --cmdline /etc/kernel/fallback_cmdline"
```

### Install systemd-boot

Make sure the system has booted in UEFI mode and that UEFI variables are accessible:

```sh
ls /sys/firmware/efi/efivars
```

Use `bootctl` to install systemd-boot into the EFI system partition:

```sh
bootctl install
```

### Configure systemd-boot

`/efi/loader/loader.conf`:

```
timeout 5
editor no
console-mode keep
```

Generate Unified Kernel Images:

```sh
mkinitcpio -P
```

## Install microcode

> Processor manufacturers release stability and security updates to the processor microcode. These updates provide bug fixes that can be critical to the stability of your system. Without them, you may experience spurious crashes or unexpected system halts that can be difficult to track down.

> All users with an AMD or Intel CPU should install the microcode updates to ensure system stability. 

```sh
pacman -S intel-ucode
```

## Setup Networking

Enable systemd-networkd and systemd-resolved

```sh
systemctl enable systemd-networkd
systemctl enable systemd-resolved
```

Configure your Networks, some expamples:

> source and more information: [Arch Wiki](https://wiki.archlinux.org/title/Systemd-networkd#Configuration_examples)

### Simple Wired Network with DHCP

In `/etc/systemd/network/20-wired.network`:

```
[Match]
Name=enp1s0

[Network]
DHCP=yes
```

### Simple Wired Network with Static IP

In `/etc/systemd/network/20-wired.network`:

```
[Match]
Name=enp1s0

[Network]
[Network]
Address=10.1.10.9/24
Gateway=10.1.10.1
DNS=10.1.10.1
```

### Simple Wireless Network with DHCP

In `/etc/systemd/network/25-wireless.network`:

```
[Match]
Name=wlp2s0

[Network]
DHCP=yes
IgnoreCarrierLoss=3s
```

## Installing Secure Boot

\* You need to boot in the freshly installed OS (without chroot) before following these steps.

Before you proceed, beware of [this](https://www.reddit.com/r/archlinux/comments/pec41w/secure_boot_selfsigned_keys_nvidia_gpu_bricked/).

> Secure Boot is a security feature found in the UEFI standard, designed to add a layer of protection to the pre-boot process: by maintaining a cryptographically signed list of binaries authorized or forbidden to run at boot, it helps in improving the confidence that the machine core boot components (boot manager, kernel, initramfs) haven't been tampered with.

1. Clear existing keys and reset Secure Boot to `Setup Mode` on firmware settings. (`Secure Boot: Enabled`, `Microsoft CA: Disabled`, `Mode: Audit Mode`)
2. `pacman -S sbctl`
3. `sbctl status`

```
Installed:    Sbctl is not installed
Setup Mode:   Enabled
Secure Boot:  Disabled
```

4. `sbctl create-keys`
5. `sbctl enroll-keys -m` -> `sbctl status`

```
Installed:    Sbctl is installed
Owner GUID:   xxx
Setup Mode:   Disabled
Secure Boot:  Disabled
```

6. `sbctl verify` -> `sbctl sign -s <file>`
> Since we are using systemd-boot we have to sign the bootloader in /usr/lib 
> `sbctl sign -s -o /usr/lib/systemd/boot/efi/systemd-bootx64.efi.signed /usr/lib/systemd/boot/efi/systemd-bootx64.efi`
> see: [Arch Wiki](https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface/Secure_Boot#Automatic_signing_with_the_pacman_hook)
7. `sbctl list-files`

## Using TPM 2.0

> Trusted Platform Module (TPM) is an international standard for a secure cryptoprocessor, which is a dedicated microprocessor designed to secure hardware by integrating cryptographic keys into devices. 

Check for support:

```sh
cat /sys/class/tpm/tpm0/tpm_version_major
```

Install required packages for management:

```sh
pacman -S tpm2-tss tpm2-tools
```

List available TPMs:

```sh
systemd-cryptenroll --tpm2-device=list
```

```
PATH        DEVICE     DRIVER
/dev/tpmrm0 NTC0702:00 tpm_tis
```

> Platform Configuration Registers (PCR) contain hashes that can be read at any time but can only be written via the extend operation, which depends on the previous hash value, thus making a sort of blockchain. They are intended to be used for platform hardware and software integrity checking between boots (e.g. protection against Evil Maid attack). They can be used to unlock encryption keys and proving that the correct OS was booted.

See [Accessing PCR registers](https://wiki.archlinux.org/title/Trusted_Platform_Module#Accessing_PCR_registers).

Enroll the key in the TPM and the LUKS volume and bind the key to PCRs 0 and 7:

```sh
systemd-cryptenroll --tpm2-device=auto --tpm2-pcrs=0+7 /dev/nvme0n1p2
```

* `PCR0`:	Core System Firmware executable code (aka Firmware)
* `PCR7`: Secure Boot State

```
New TPM2 token enrolled as key slot 1.
```

Test that the key can open the volume:

```sh
/usr/lib/systemd/systemd-cryptsetup attach swap /dev/nvme0n1p2 - tpm2-device=auto
```

If you wish to remove LUKS keys from TPM: `systemd-cryptenroll /dev/disk/by-partlabel/cryptswap --wipe-slot=tpm2`

## TPM auto unlock for SWAP

Edit `/etc/kernel/cmdline`:

* `<ROOT-PARTITION-UUID>`: `lsblk -o NAME,UUID | grep nvme0n1p3 | awk '{print $NF}'`
* `<SWAP-PARTITION-UUID>`: `lsblk -o NAME,UUID | grep nvme0n1p2 | awk '{print $NF}'`

```
rw quiet bgrt_disable rd.luks.name=<UUID-of-swap-luks-container>=swap rd.luks.options=<UUID-of-swap-luks-container>=tpm2-device=auto rd.luks.name=<UUID-of-root-luks-container>=root root=/dev/mapper/root resume=/dev/mapper/swap
```

Edit `/etc/kernel/fallback_cmdline`:

```
rw debug bgrt_disable rd.luks.name=<UUID-of-swap-luks-container>=swap rd.luks.options=<UUID-of-swap-luks-container>=tpm2-device=auto rd.luks.name=<UUID-of-root-luks-container>=root root=/dev/mapper/root
```

Regenerate Unified Kernel Images:

```sh
mkinitcpio -P
```

# Post-installation

## Set up a wireless netwok

Edit `/var/lib/iwd/<NAME-OF-SSID>.psk`:

```
[Security]
Passphrase=<PASSPHRASE-OF-WIFI-NETWORK>
```

The Passphrase gets encrypted on first connect and can then be removed from the file.


Edit `/etc/systemd/network/20-wifi.network`:

```
[Match]
Name=wlan0
SSID=<SSID-OF-WIFI-NETWORK>

[Network]
DHCP=yes

[DHCPv4]
UseDNS=true
UseDomains=true
```

The `SSID=` parameter is optional, if you want to set diffrent behavior for diffrent SSIDs.

## Update pacman mirrors

_(this section needs review)_

```sh
sudo pacman -S reflector
reflector --country Germany --age 12 --protocol https --sort rate --save /etc/pacman.d/mirrorlist
sudo vim /etc/pacman.d/mirrorlist
```

## Install window manager

```sh
pacman -S i3
```

Configure i3 and run it with [xinit](https://wiki.archlinux.org/title/Xinit).

## Set up audio

Check the audio device:

```sh
lspci | grep -i audio
```

Install pipewire/pulse:

```sh
pacman -S alsa-utils pipewire pipewire-pulse
systemctl start --user pipewire-pulse.service
pactl info
```

Set up bluetooth:

```sh
rfkill unblock all
pacman -S bluez bluez-utils
systemctl start bluetooth.service
systemctl enable bluetooth.service
```

Update `/etc/bluetooth/main.conf` to auto power-on the bluetooth device after boot:

```
[Policy]
AutoEnable=true
```

_Dont know yet_

Configure bluetooth headset:

```
bluetoothctl

[bluetooth]# power on
[CHG] Controller XX:XX:XX:XX:XX:XX Class: 0x006c010c
Changing power on succeeded
[CHG] Controller XX:XX:XX:XX:XX:XX Powered: yes

[bluetooth]# agent on
Agent is already registered
[bluetooth]# default-agent
Default agent request successful

[bluetooth]# scan on
Discovery started
[CHG] Controller XX:XX:XX:XX:XX:XX Discovering: yes
[NEW] Device E8:D0:3C:8B:7B:48 JBL TUNE500BT

[bluetooth]# pair E8:D0:3C:8B:7B:48 
Attempting to pair with E8:D0:3C:8B:7B:48
[CHG] Device E8:D0:3C:8B:7B:48 Connected: yes
[CHG] Device E8:D0:3C:8B:7B:48 ServicesResolved: yes
[CHG] Device E8:D0:3C:8B:7B:48 Paired: yes
Pairing successful
[CHG] Device E8:D0:3C:8B:7B:48 ServicesResolved: no
[CHG] Device E8:D0:3C:8B:7B:48 Connected: no

[bluetooth]# connect E8:D0:3C:8B:7B:48 
Attempting to connect to E8:D0:3C:8B:7B:48
[CHG] Device E8:D0:3C:8B:7B:48 Connected: yes
Connection successful
[CHG] Device E8:D0:3C:8B:7B:48 ServicesResolved: yes

[JBL TUNE500BT]# trust E8:D0:3C:8B:7B:48 
[CHG] Device E8:D0:3C:8B:7B:48 Trusted: yes
Changing E8:D0:3C:8B:7B:48 trust succeeded

[JBL TUNE500BT]# scan off
[JBL TUNE500BT]# exit
```

Update `/etc/pulse/default.pa` for auto connecting to the bluetooth headset:

```
### Automatically switch to newly-connected devices
load-module module-switch-on-connect
```

## Change TrackPoint sensitivity

Install `xinput` for configuring devices:

```sh
pacman -S xorg-xinput xf86-input-libinput
```

List the available devices by running `xinput` command:

```
⎡ Virtual core pointer                    	id=2	[master pointer  (3)]
⎜   ↳ Virtual core XTEST pointer              	id=4	[slave  pointer  (2)]
⎜   ↳ ETPS/2 Elantech Touchpad                	id=12	[slave  pointer  (2)]
⎜   ↳ ETPS/2 Elantech TrackPoint              	id=13	[slave  pointer  (2)]
```

List the properties of the TrackPoint:

```sh
xinput list-props "ETPS/2 Elantech TrackPoint"
```

```
[...]
libinput Accel Speed (316):	0.000000
libinput Accel Speed Default (317):	0.000000
libinput Accel Profiles Available (318):	1, 1
[...]
```

Override the `libinput Accel Speed` property in `/etc/X11/xorg.conf.d/20-thinkpad.conf`:

```conf
Section "InputClass"
    Identifier "TrackPoint Configuration"
    MatchProduct "ETPS/2 Elantech TrackPoint"
    Option "AccelSpeed"	"-0.65"
EndSection
```

## References

- https://wiki.archlinux.org/title/Installation_guide
- https://wiki.archlinux.org/title/User:Altercation/Bullet_Proof_Arch_Install
- https://wiki.archlinux.org/title/GPT_fdisk
- https://wiki.archlinux.org/title/Dm-crypt/Device_encryption
- https://wiki.archlinux.org/title/Dm-crypt/Swap_encryption
- https://wiki.archlinux.org/title/Trusted_Platform_Module#Data-at-rest_encryption_with_LUKS
- https://wiki.archlinux.org/title/Systemd-boot
- https://wiki.archlinux.org/title/Microcode
- https://wiki.archlinux.org/title/Power_management/Suspend_and_hibernate
- https://wiki.archlinux.org/title/Unified_Extensible_Firmware_Interface/Secure_Boot
- https://wiki.archlinux.org/title/Netctl
- https://github.com/Foxboron/sbctl
- https://wiki.archlinux.org/title/PipeWire
- https://wiki.archlinux.org/title/bluetooth_headset
- https://wiki.archlinux.org/title/Bluetooth#Auto_power-on_after_boot
- https://wiki.archlinux.org/title/TrackPoint

